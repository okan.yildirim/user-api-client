package main

import (
	"context"
	"fmt"
	"github.com/bxcodec/faker/v3"
	"userapiclient/pkg"
)

func main() {
	client := pkg.NewClient()
	ctx := context.Background()

	// create
	userRequest := getRandomUser()
	user, err := client.CreateUser(ctx, userRequest)
	handleError(err)
	fmt.Println("Created User", *user)

	// get All
	users, err := client.GetUsers(ctx)
	handleError(err)
	fmt.Println("All Users", *users)

	// update
	userRequest.Age = 100
	updatedUser, err := client.UpdateUser(ctx, userRequest, (*user).Id)
	handleError(err)
	fmt.Println("Updated Users", *updatedUser)
	// get
	fetchedUser, err := client.GetUser(ctx, user.Id)
	handleError(err)
	fmt.Println("Fetched User", *fetchedUser)

	//delete
	err = client.DeleteUser(ctx, user.Id)
	handleError(err)

	// get All
	users, err = client.GetUsers(ctx)
	handleError(err)
	fmt.Println("All Users", *users)

}

func handleError(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func getRandomUser() pkg.UserRequest {
	intInRange, _ := faker.RandomInt(3, 99)

	return pkg.UserRequest{
		Name:     faker.FirstName(),
		LastName: faker.LastName(),
		Age:      intInRange[0],
	}
}
