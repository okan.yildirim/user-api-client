package pkg

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"
)

const host = "http://localhost:9000"

type Client struct {
	BaseURL string
	// apiKey     string
	HTTPClient *http.Client
}

func NewClient() *Client {
	return &Client{
		BaseURL:    host,
		HTTPClient: &http.Client{Timeout: time.Minute},
	}
}

type errorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type successResponse struct {
	Code int         `json:"code"`
	Data interface{} `json:"data"`
}

type PageOptions struct {
	Limit int `json:"limit"`
	Page  int `json:"page"`
}

func (c *Client) GetUsers(ctx context.Context) (*[]UserResponse, error) {

	request, err := http.NewRequest("GET", fmt.Sprintf("%s/users", c.BaseURL), nil)
	if err != nil {
		return nil, err
	}
	request.WithContext(ctx)

	var userResponses []UserResponse
	err = c.sendRequest(request, &userResponses)
	if err != nil {
		return nil, err
	}

	return &userResponses, nil
}
func (c *Client) GetUser(ctx context.Context, id uint) (*UserResponse, error) {
	request, err := http.NewRequest("GET", fmt.Sprintf("%s/users/%d", c.BaseURL, id), nil)
	if err != nil {
		return nil, err
	}
	request.WithContext(ctx)

	var userResponse UserResponse
	c.sendRequest(request, &userResponse)

	return &userResponse, nil
}

func (c *Client) CreateUser(ctx context.Context, userRequest UserRequest) (*UserResponse, error) {
	reqBody, _ := json.Marshal(userRequest)
	request, err := http.NewRequest("POST", fmt.Sprintf("%s/users", c.BaseURL), bytes.NewBuffer(reqBody))

	if err != nil {
		return nil, err
	}
	request.WithContext(ctx)
	var userCreateResponse UserResponse

	err = c.sendRequest(request, &userCreateResponse)
	if err != nil {
		return nil, err
	}
	return &userCreateResponse, nil
}

func (c *Client) UpdateUser(ctx context.Context, userRequest UserRequest, id uint) (*UserResponse, error) {
	reqBody, _ := json.Marshal(userRequest)
	request, err := http.NewRequest("PUT", fmt.Sprintf("%s/users/%d", c.BaseURL, id), bytes.NewBuffer(reqBody))

	if err != nil {
		return nil, err
	}
	request.WithContext(ctx)
	var userCreateResponse UserResponse

	err = c.sendRequest(request, &userCreateResponse)
	if err != nil {
		return nil, err
	}

	return &userCreateResponse, nil
}

func (c *Client) DeleteUser(ctx context.Context, id uint) error {
	request, err := http.NewRequest("DELETE", fmt.Sprintf("%s/users/%d", c.BaseURL, id), nil)
	if err != nil {
		return err
	}
	request.WithContext(ctx)

	return c.sendRequest(request, nil)
}

func (c *Client) sendRequest(request *http.Request, response interface{}) error {

	request.Header.Set("Content-Type", "application/json")
	request.Header.Set("Accept", "application/json; charset=utf-8")
	// request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.apiKey))

	res, err := c.HTTPClient.Do(request)

	if err != nil {
		return err
	}

	defer res.Body.Close()

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errorResponse errorResponse
		if err = json.NewDecoder(res.Body).Decode(&errorResponse); err != nil {
			return errors.New(errorResponse.Message)
		}
		return fmt.Errorf("unknown error, status code: %d", res.StatusCode)
	}

	if response != nil {
		if err = json.NewDecoder(res.Body).Decode(response); err != nil {
			return err
		}
	}
	return nil
}
