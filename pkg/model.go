package pkg

type UserRequest struct {
	Name string `json:"name"`
	LastName string `json:"lastName"`
	Age int `json:"age"`
}

type UserResponse struct {
	Id uint `json:"id"`
	Name string `json:"name"`
	LastName string `json:"lastName"`
	Age int `json:"age"`
}